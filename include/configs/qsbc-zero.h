// SPDX-License-Identifier: GPL-2.0-or-later
//
// Copyright (C) 2022 Quentin Freimanis.
#ifndef __QSBC_ZERO_CONFIG_H
#define __QSBC_ZERO_CONFIG_H

#include <asm/arch/imx-regs.h>
#include <linux/sizes.h>
#include <linux/stringify.h>
#include "mx6_common.h"
#include <asm/mach-imx/gpio.h>

#define PHYS_SDRAM_SIZE	SZ_512M

#define CONFIG_MXC_UART_BASE		UART1_BASE
#define CONFIG_SYS_FSL_ESDHC_ADDR	USDHC1_BASE_ADDR

#define CONFIG_EXTRA_ENV_SETTINGS \
	"image=zImage\0" \
	"console=ttymxc0\0" \
	"baudrate=115200\0" \
	"fdtfile=imx6ull-qsbc-zero.dtb\0" \
	"fdtaddr=0x83000000\0" \
	"mmcdev=0\0" \
	"mmcpart=1\0" \
	"mmcroot=/dev/mmcblk0p2\0" \
	\
	"mmcargs=setenv bootargs console=${console},${baudrate} " \
		"root=${mmcroot}\0" \
	"loadimage=fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${image}\0" \
	"loadfdt=fatload mmc ${mmcdev}:${mmcpart} ${fdtaddr} dtbs/${fdtfile}\0" \
	"loadfdtold=fatload mmc ${mmcdev}:${mmcpart} ${fdtaddr} imx6ull-qsbc-zero.dtb\0" \
	\
	"mmcboot=echo Booting from mmc ...; " \
			"run mmcargs; " \
			"if run loadimage; then " \
				"if run loadfdt; then " \
					"bootz ${loadaddr} - ${fdtaddr} " \
				"else " \
					"echo Failed to load DT from dtbs dir; " \
					"if run loadfdtold; then " \
						"bootz ${loadaddr} - ${fdtaddr} " \
					"else " \
						"echo Failed to load DT; " \
				"fi; " \
			"else " \
				"echo Failed to load zImage; " \
			"fi;\0" \
	\
	"bootcmd=run mmcboot;\0"

/* Miscellaneous configurable options */

/* Physical Memory Map */
#define PHYS_SDRAM			MMDC0_ARB_BASE_ADDR

#define CFG_SYS_SDRAM_BASE		PHYS_SDRAM
#define CFG_SYS_INIT_RAM_ADDR	IRAM_BASE_ADDR
#define CFG_SYS_INIT_RAM_SIZE	IRAM_SIZE

#endif
